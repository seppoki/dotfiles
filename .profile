# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
	  . "$HOME/.bashrc"
  fi
fi

# prompt
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
  debian_chroot=$(cat /etc/debian_chroot)
fi  
PS1="\[\033[01;34m\]${debian_chroot:+($debian_chroot)}\u\[\033[00m\]@\[\033[01;34m\]\h\[\033[00m\]:\[\033[01;32m\]\w\[\033[00m\]"

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
PS1+=":\[\033[01;31m\]\$(parse_git_branch)\[\033[00m\]$ "

# default programs
export TERMINAL=alacritty
export EDITOR=nvim
export BROWSER=chromium

# locale
export LANGUAGE=en_GB.UTF-8
export LANG=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8

if [ -d "$HOME/bin" ] ; then
  PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
  PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/scripts" ] ; then
  PATH="$HOME/scripts:$PATH"
fi

if [ -d "/usr/local/go/bin" ] ; then
  PATH="/usr/local/go/bin:$PATH"
fi

# zig compiler and language tools
# PATH="$HOME/zig-linux-x86_64-0.10.0-dev.2377+71e2a56e3:$PATH"
PATH="$HOME/zig-linux-x86_64-0.11.0-dev.945+0c30e006c:$PATH"
PATH="$HOME/zls/zig-out/bin:$PATH"

# opam configuration
test -r /home/seppoki/.opam/opam-init/init.sh && . /home/seppoki/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true

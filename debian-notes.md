# Debian system notes

Personal notes for debian server and workstation installation, configuration and general upkeeping.

## workstation

### graphical environment clean install

start with a minimal netinst

check sources at /etc/apt/sources.list and add components if needed

    contrib non-free

#### environment

* xorg
* xutils

#### xmonad

xmonad.org/INSTALL.html

required packages:
build-essential curl libffi-dev libffi8 libgmp-dev libgmp10 libncurses-dev libncurses5 libtinfo5

#### dwm

* libxft-dev
* libxinerama-dev
* libx11-dev
* libxcb1-dev
* libxcb-res0-dev

clone and install patched dwm

    git clone https://gitlab.com/seppoki/dwm
    cd dwm
    git checkout setup
    sudo make install clean

### system packages

microcode security updates

* intel-microcode / amd-microcode

check that devices are rocognized correctly

    lspci

check for firmware errors

    sudo dmesg

install firware packages if required

* firmware-linux
* firmware-linux-nonfree
* firmware-misc-nonfree
* firmware-realtek
* firmware-iwlwifi

### graphics card

https://wiki.debian.org/GraphicsCard

### general util packages

* git
* curl
* vim
* tmux
* htop
* powertop
* psmisc
* at --no-install-recommends
* nnn
* dmenu
* fzf
* ripgrep
* fd-find
    - named fdfind on debian, link to fd
    - ln -s $(which fdfind) ~/.local/bin/fd
* xclip
* xcape
* feh
* earlyoom
* rofi
* scrot
* sxhkd
* alsamixer
* alsa-utils
* bc
* zathura
* mupdf
    - support for epub

### dev packages

* build-essential

### swap file

create the file, ie. 4 GiB

    sudo dd if=/dev/zero of=/swapfile bs=1024 count=4194304
    sudo chmod 600 /swapfile
    
make it a swap file

    sudo mkswap /swapfile
    sudo swapon /swapfile

add fstab entry to mount swapfile on boot

    /swapfile       none    swap    sw      0       0

### compressed ram swap with zram

install zram-tools

    sudo apt install zram-tools

edit /etc/default/zramswap as desired

    ALGO=lz4
    PERCENT=200

start swap with

    sudo gramswap status
    sudo zramswap start

set better suiting settings to /etc/sysctl.conf

   sudo echo "vm.swappiness = 100" >> /etc/sysctl.conf
   sudo echo "vm.page-cluster = 0" >> /etc/sysctl.conf

### mounting hard drive

find disk UUID

    sudo fdisk -l
    sudo blkid

add a mount entry

    sudo vim /etc/fstab

### cloning dotfiles

    git clone --bare https://gitlab.com/seppoki/dotfiles.git $HOME/dotfiles
    alias dotfiles='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
    dotfiles checkout
    dotfiles config --local status.showUntrackedFiles no

### adding modified keyboard layout

    sudo cp .config/finner /usr/share/X11/xkb/symbols/
    sudo cp .config/filemak-dh /usr/share/X11/xkb/symbols/

optionally add .xml layout sections to /usr/share/X11/xkb/rules/evdev.xml

### disabling doots

    sudo cp $HOME/.config/nobeep.conf /etc/modprobe.d/

### locales

    sudo dpkg-reconfigure locales

### mouse in X

    sudo cp $HOME/.config/50-mouse-acceleration.conf /usr/share/X11/xorg.conf.d/

### fonts

drop-in replacement for ms fonts

* fonts-liberation2

terminal font of choise
    
    mkdir $HOME/FiraMono && cd $HOME/FiraMono
    sudo curl -Lo FiraMono.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip
    sudo apt install unzip
    unzip FiraMono.zip
    sudo mv $HOME/FiraMono /usr/share/fonts/
    sudo apt install fontconfig

if fontconfig installation didnt recreate cache

    sudo fc-cache -v

adjust font rendering

    sudo /usr/sbin/dpkg-reconfigure fontconfig-config

### notification with wm

to receive and show notifications in wm

* dunst

to send notification with notify-send

* libnotify-bin

### screen treaing

to fix tearing with a wm

* picom

### trash bin

* trash-cli

### triggering actions on inactivity

* xautolock

### clipboard

https://github.com/cdown/clipnotify

    sudo apt install xsel libxfixes-dev
    git clone https://github.com/cdown/clipnotify
    cd clipnotify
    sudo make install clean

https://github.com/cdown/clipmenu

    git clone https://github.com/cdown/clipmenu
    cd clipmenu
    sudo make install

### support for appimages

    sudo apt install fuse
    sudo modprobe -v fuse
    sudo addgroup fuse
    sudo adduser $USER fuse

### docker

https://docs.docker.com/engine/install/debian/#install-using-the-repository

### neovim nightly

https://github.com/neovim/neovim/releases

    curl -L -o nvim https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
    chmod a+x nvim
    sudo mv nvim /usr/local/bin/

packer

https://github.com/wbthomason/packer.nvim

    git clone https://github.com/wbthomason/packer.nvim ~/.local/share/nvim/site/pack/packer/start/packer.nvim

### rust

https://rust-lang.github.io/rustup/installation/other.html

    curl https://sh.rustup.rs -sSf | sh -s -- --no-modify-path

package to update installed packages

    cargo install cargo-update

updating packages

    cargo install-update -a

### alacritty

https://github.com/alacritty/alacritty/blob/master/INSTALL.md#debianubuntu

    sudo apt install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3
    cargo install alacritty

check that terminfo is installed

    infocmp alacritty

in gnome desktop to add lauch entry

    sudo cp alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
    sudo desktop-file-install Alacritty.desktop
    sudo update-desktop-database

if alacritty does not show up, add a softlink to path

    sudo ln -s $HOME/.cargo/bin/alacritty /usr/local/bin/alacritty

set alacritty as default terminal

    sudo update-alternatives --install $(which x-terminal-emulator) x-terminal-emulator $(which alacritty) 40
    sudo update-alternatives --config x-terminal-emulator

### fnm

https://github.com/Schniz/fnm

cargo install fnm

### golang

https://golang.org/doc/install

    curl -LO https://golang.org/dl/go1.16.3.linux-amd64.tar.gz
    sudo tar -C /usr/local -xzf go1.16.3.linux-amd64.tar.gz

### wallpaper

wallpaper can be set with feh

    feh --bg-scale <path>

### dwm status

add status updating script to crontab

    crontab -e

    * * * * * /home/seppoki/scripts/dwm_status > /dev/null 2>&1


### gui applications

* libgtk-3-0
* lxappearance
* pulseaudio

### firefox

* firefox-esr

or for a newer version:

https://www.mozilla.org/en-US/firefox/all/#product-desktop-release

    curl -L -o firefox.tar.bz2 'https://download.mozilla.org/?product=firefox-beta-latest-ssl&os=linux64&lang=en-US'

uncompress

    sudo tar xjf firefox.tar.bz2 -C /opt/

symlink

    sudo ln -s /opt/firefox/firefox /usr/bin/firefox

### powertop

    sudo powertop --html=report.html

### ssh keys

generate keys on client

    ssh-keygen -t rsa -b 4096 -C "comment"

copy public key to server

    ssh-copy-id user@host

### flatpak

    sudo apt install flatpak
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

with minimal non de installation need additional package

* dbus-x11

and additionally before launching gui app

    eval $(dbus-launch --sh-syntax)

#### chromium

flatpak install org.chromium.Chromium

#### discord

    flatpak install flathub com.discordapp.Discord

to start minimized add to autostart file

    --start-minimized

#### telegram

    flatpak install flathub org.telegram.desktop

### dropbox

https://www.dropbox.com/install-linux

    wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
    sudo ln -s $HOME/.dropbox-dist/dropboxd /usr/local/bin/dropboxd

### vscode

    sudo apt install software-properties-common apt-transport-https curl
    curl -sSL https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
    sudo apt update
    sudo apt install code

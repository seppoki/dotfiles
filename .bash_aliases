# bare dotfiles repo
alias dg='/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME'
alias de='GIT_DIR=$HOME/dotfiles GIT_WORK_TREE=$HOME nvim'

# colored ls
alias ls='ls --color=auto'

alias :q=exit

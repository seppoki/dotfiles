import XMonad
import XMonad.Util.EZConfig
import qualified XMonad.Util.Hacks as Hacks
import XMonad.Hooks.InsertPosition
import XMonad.Layout.NoBorders
import XMonad.Hooks.WindowSwallowing
import qualified XMonad.Actions.FlexibleResize as Flex

-- place new windows to the bottom of the stack
toStackBottom = setupInsertPosition End Newer

eventHook = composeAll [
    -- fix chrome window sizing when full screening
    Hacks.windowedFullscreenFixEventHook
    -- replace alacritty window when launching windowed program
  , swallowEventHook (className =? "Alacritty") (return True) ]

main :: IO ()
main = xmonad $ toStackBottom $ def
    { modMask = mod4Mask  -- Rebind Mod to the Super key
    , terminal = "alacritty"
    , borderWidth = 2
    , normalBorderColor = "#131313"
    , focusedBorderColor = "#e9724c"
    , handleEventHook = eventHook
    , layoutHook = Tall 1 (2/100) (3/5) ||| noBorders Full
    }
  `additionalKeysP`
    [ ("M-r", spawn "dmenu_run -l 25")
    , ("M-S-b", spawn "chromium")
    , ("M-S-t", spawn "telegram")
    , ("M-S-d", spawn "discord")
    , ("M-z", spawn "show_time")
    , ("M-.", spawn "volume_increase")
    , ("M-,", spawn "volume_decrease")
    ]
  `additionalMouseBindings`
    [   -- grab nearest corner when resizing
       ((mod4Mask, button3), (\w -> focus w >> Flex.mouseResizeWindow w))
    ]

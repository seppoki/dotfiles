" encoding
set encoding=UTF-8 " internal encoding
" set fileencoding=UTF-8 " written encoding

" cs3110
set rtp^="/home/seppoki/.opam/cs3110-2023sp/share/ocp-indent/vim"

"
let mapleader = " "
let maplocalleader = " "

" behaviour
set clipboard^=unnamed,unnamedplus " sync with copy and selection clipboard
set hidden " opening a new file hides current buffer
set undodir=~/.vim/undodir
set undofile
set noswapfile
set autoread " reload modified file automatically
set cmdheight=1 " space for messages
set updatetime=250 " update more frequently
set backspace=indent,eol,start " allow backspacing over everything
set history=5000 " buffer size
set shortmess+=c " dont give ins-completion-menu messages
set completeopt=menuone,preview,noinsert,noselect " completion menu behaviour
set scrolloff=3 " scrolling trigger distance
set sidescrolloff=4 " horizontal scroll trigger distance
set wrap " visually wrap lines
set linebreak " avoid wrapping mid-word
" set lazyredraw " dont draw while executing
set t_ut= " disable background color erase
set mouse=a " enable mouse on all modes
set timeoutlen=1000 " wait for mapped sequence to complete
set wildmenu " enable wildmenu
set wildmode=longest:full,full " first tab matches longest and opens list
filetype plugin on " load filetype plugins

" visual
set showmode " display current mode
set ruler " display current position
set title " make window title reflect the edited file
set cursorline " highlight current line
set number " show line numbers
" set numberwidth=3 " number column width
" set relativenumber " line numbers are indexed based on current line
set showmatch " show matching parentshesis
hi normal ctermbg=NONE guibg=NONE " help with flickering inside tmux
set colorcolumn=81 " vertical line after 80th character
set pumheight=10 " pop up menu height
set signcolumn=auto " auto expands signcolumn as needed
set laststatus=3 " 3 for global statusline
set matchtime=2 " matching pair flash n100ms
" show a set of invisible characters
set list
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·
" color scheme
set termguicolors " advanced coloring
set background=dark " hint about background color
" syntax on " lexical highlighting
let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_menu_selection_background = 'orange'
let g:gruvbox_material_sign_column_background = 'none'
let g:gruvbox_material_diagnostic_virtual_text = 1
let g:gruvbox_material_statusline_style = 'original'
let g:gruvbox_material_palette = 'mix'
let g:gruvbox_material_better_performance = 1
let g:gruvbox_material_ui_contrast = 'high'
" let g:gruvbox_material_diagnostic_text_highlight = 1
let g:gruvbox_material_diagnostic_virtual_text = 'colored'
" colorscheme gruvbox-material " select color scheme

" search
set hlsearch " hightlight search matches
set incsearch " highlight as you type
set ignorecase " ignore case
set smartcase " dont ignore case whan inserting capital letters

" indentation
set noexpandtab " insert tabs instead of a spacespaces
set tabstop=4 " tab is # spaces
set shiftwidth=0 " number or spaces inserted on indentation, 0 for tabstop
set softtabstop=-1 " when shifting text, -1 for shiftwidth
set shiftround " align indentations
set autoindent " indent new lines automatically
filetype indent on " load indentation files

" split
set splitbelow " lower split starts active
set splitright " right split starts active

" diff
" set diffopt+=algorithm:histogram,indent-heuristic
set diffopt+=linematch:50

lua << EOF
  require'plugins'
EOF

" make current line number pop out
" hi CursorLineNr guibg=orange

" reselect visual after shifting
vnoremap < <gv
vnoremap > >gv

"" shuffling movement to better fit layout
"noremap h j
"noremap j h
"noremap k l
"noremap l k

" fzf-lua
nnoremap <silent> <Leader>ff <cmd>FzfLua files<CR>
nnoremap <silent> <Leader>fg <cmd>FzfLua grep<CR>
nnoremap <silent> <Leader>fb <cmd>FzfLua buffers<CR>
nnoremap <silent> <Leader>a <cmd>FzfLua lsp_code_actions<CR>

" undotree
nnoremap <silent> <leader>u :UndotreeToggle<CR>

" reload modified file
autocmd! FocusGained,BufEnter * checktime

" highlight cursor line only on active window
augroup CursorLineHighlight
    autocmd!
    autocmd WinEnter,BufEnter,FocusGained * set cursorline
    autocmd WinLeave,BufLeave,FocusLost * set nocursorline
augroup END

" make triling spaces conspicuous
match TrailingSpaces '\s\+$'
augroup TrailingSpaceHighlight
    autocmd!
    autocmd InsertEnter * highligh link TrailingSpaces NonText
    autocmd InsertLeave * highligh link TrailingSpaces ErrorMsg
augroup END

" hide line numbers in terminal
autocmd TermOpen * setlocal nonumber norelativenumber

" auto equal windows on window resize
autocmd VimResized * wincmd =

" spell check git commits
autocmd FileType gitcommit setlocal spell

" diagtostic information as colored line numbers
sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl=DiagnosticSignError
sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=DiagnosticSignWarn
sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl=DiagnosticSignInfo
sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl=DiagnosticSignHint

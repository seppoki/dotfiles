local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	vim.fn.system({
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/folke/lazy.nvim.git",
		"--branch=stable",
		lazypath,
	})
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
	{
		'sainnhe/gruvbox-material',
		lazy = false,
		priority = 1000,
		config = function()
			vim.cmd([[colorscheme gruvbox-material]])
		end
	},
	{
		'echasnovski/mini.nvim', version = '*',
		config = function()
			require('mini.pairs').setup()
		end
	},
	{
		'tpope/vim-fugitive',
		cmd = "G"
	},
	'tpope/vim-surround',
	"chaoren/vim-wordmotion",
	'tpope/vim-repeat',
	-- 'tpope/vim-sleuth',
	-- 'eraserhd/parinfer-rust',
	-- {
	-- 	"danymat/neogen",
	-- 	dependencies = "nvim-treesitter/nvim-treesitter",
	-- 	cmd = "Neogen",
	-- 	config = function()
	-- 		require('neogen').setup({ snippet_engine = "snippy" })
	-- 	end,
	-- },
	-- {
	-- 	'kkoomen/vim-doge',
	-- 	build = ":call doge#install()",
	-- },
	{
		'mbbill/undotree',
		cmd = "UndotreeToggle"
	},
	{
		'https://git.sr.ht/~whynothugo/lsp_lines.nvim',
		config = function()
			require('lsp_lines').setup()
			vim.keymap.set(
				"",
				"<Leader>l",
				require("lsp_lines").toggle,
				{ desc = "Toggle lsp_lines" }
			)
		end
	},
	{
		'ibhagwan/fzf-lua',
		cmd = "FzfLua",
		dependencies = { 'kyazdani42/nvim-web-devicons', },
		config = function()
			require('fzf-lua').setup({
				fzf_opts = {
					['--border'] = 'none'
				},
				winopts = { fullscreen = true },
			})
		end
	},
	{
		'numToStr/Comment.nvim',
		lazy = false,
	},
	{
		'lewis6991/gitsigns.nvim',
		event = "BufReadPre",
		-- dependencies = { 'nvim-lua/plenary.nvim' },
		config = function()
			require('gitsigns').setup({
				preview_config = { border = 'none' },
			})
		end
	},
	{
		'nvim-treesitter/nvim-treesitter',
		build = ":TSUpdate",
		event = "BufReadPost",
		config = function()
			require('nvim-treesitter.configs').setup({
				ensure_installed = "all",
				highlight = { enable = true },
				indent = { enable = true }
			})
		end
	},
	{
		"stevearc/oil.nvim",
		dependencies = { 'kyazdani42/nvim-web-devicons', },
		config = function()
			require('oil').setup()
			vim.keymap.set("n", "-", require('oil').open, { desc = "Open parent directory" })
		end
	},
	{
		'williamboman/mason-lspconfig.nvim',
		dependencies = {
			-- completion
			'hrsh7th/cmp-nvim-lsp',
			'hrsh7th/cmp-buffer',
			'hrsh7th/nvim-cmp',
			'hrsh7th/cmp-nvim-lsp-signature-help',
			-- lsp
			'williamboman/mason.nvim',
			'neovim/nvim-lspconfig',
			-- snippets
			'dcampos/nvim-snippy',
			'honza/vim-snippets',
			'dcampos/cmp-snippy',
		},
		config = function()
			require('snippy').setup({
				mappings = {
					is = {
						['<Tab>'] = 'expand_or_advance',
						['<S-Tab>'] = 'previous',
					},
					nx = {
						['<leader>x'] = 'cut_text',
					},
				}
			})

			local cmp = require('cmp')
			cmp.setup({
				mapping = cmp.mapping.preset.insert({
					['<C-e>'] = cmp.mapping.abort(),
					['<CR>'] = cmp.mapping.confirm({ select = false }),
				}),
				sources = cmp.config.sources({
					{ name = 'nvim_lsp' },
					{ name = 'snippy' },
					{ name = 'buffer' },
					{ name = 'nvim_lsp_signature_help' },
				}),
				snippet = {
					expand = function(args)
						require('snippy').expand_snippet(args.body)
					end,
				}
			})

			local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()
			require('mason').setup()
			require('mason-lspconfig').setup()
			local lspconfig = require('lspconfig')

			local lsp_attach = function(client, bufnr)
				local opts = { noremap = true, silent = true, buffer = bufnr }
				vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
				vim.keymap.set('n', '<C-k>', vim.lsp.buf.hover, opts)
				vim.keymap.set('n', '<leader>gd', vim.lsp.buf.definition, opts)
				vim.keymap.set('n', '<leader>gr', vim.lsp.buf.rename, opts)
				vim.keymap.set('n', '<leader>F', function() vim.lsp.buf.format({ timeout_ms = 5000 }) end, opts)
				vim.keymap.set('n', '<leader>aF', function() vim.lsp.buf.format({ async = true }) end, opts)
			end

			local lsp_handlers = {
				["textDocument/publishDiagnostics"] = vim.lsp.with(
					vim.lsp.diagnostic.on_publish_diagnostics, {
						signs = true,
						underline = true,
						update_in_insert = false,
						virtual_text = false,
					}
				),
			}
			require('mason-lspconfig').setup_handlers({
				function(server_name)
					lspconfig[server_name].setup({
						on_attach = lsp_attach,
						capabilities = lsp_capabilities,
						handlers = lsp_handlers
					})
				end,
				["lua_ls"] = function()
					lspconfig.lua_ls.setup {
						on_attach = lsp_attach,
						capabilities = lsp_capabilities,
						handlers = lsp_handlers,
						settings = {
							Lua = {
								diagnostics = {
									globals = { "vim" }
								}
							}
						}
					}
				end,
			})
		end
	},
})

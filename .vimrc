set nocompatible

" encoding
set encoding=UTF-8 " internal encoding
" set fileencoding=UTF-8 " written encoding

" behaviour
set clipboard^=unnamed,unnamedplus " sync with copy and selection clipboard
set hidden " opening a new file hides current buffer
set undodir=~/.vim/undodir
set undofile
set noswapfile
set autoread " reload modified file automatically
set cmdheight=1 " space for messages
set updatetime=250 " update more frequently
set backspace=indent,eol,start " allow backspacing over everything
set history=5000 " buffer size
set shortmess+=c " dont give ins-completion-menu messages
set completeopt=menuone,preview,noinsert,noselect " completion menu behaviour
set scrolloff=4 " scrolling trigger distance
set sidescrolloff=4 " horizontal scroll trigger distance
set wrap " visually wrap lines
set linebreak " avoid wrapping mid-word
" set lazyredraw " dont draw while executing
set t_ut= " disable background color erase
set mouse=a " enable mouse on all modes
set timeoutlen=1000 " wait for mapped sequence to complete
set wildmenu " enable wildmenu
set wildmode=longest:full,full " first tab matches longest and opens list
filetype plugin on " load filetype plugins

" visual
set showmode " display current mode
set ruler " display current position
set title " make window title reflect the edited file
" set cursorline " highlight current line
set number " show line numbers
" set numberwidth=3 " number column width
" set relativenumber " line numbers are indexed based on current line
set showmatch " show matching parentshesis
hi normal ctermbg=NONE guibg=NONE " help with flickering inside tmux
set colorcolumn=81 " vertical line after 80th character
set pumheight=10 " pop up menu height
set signcolumn=auto " auto expands signcolumn as needed
set laststatus=2 " always visible status line
set matchtime=2 " matching pair flash n100ms
" show a set of invisible characters
set list
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·
" color scheme
set termguicolors " advanced coloring
set background=dark " hint about background color
syntax on " lexical highlighting

" search
set hlsearch " hightlight search matches
set incsearch " highlight as you type
set ignorecase " ignore case
set smartcase " dont ignore case whan inserting capital letters

" indentation
set noexpandtab " insert tabs instead of a spacespaces
set tabstop=4 " tab is # spaces
set shiftwidth=0 " number or spaces inserted on indentation, 0 for tabstop
set softtabstop=-1 " when shifting text, -1 for shiftwidth
set shiftround " align indentations
set autoindent " indent new lines automatically
filetype indent on " load indentation files

" split
set splitbelow " lower split starts active
set splitright " right split starts active

" diff
set diffopt+=algorithm:histogram,indent-heuristic

" make current line number pop out
hi CursorLineNr guifg=orange

" reselect visual after shifting
vnoremap < <gv
vnoremap > >gv

" reload modified file
autocmd! FocusGained,BufEnter * checktime

" auto equal windows on window resize
autocmd VimResized * wincmd =
